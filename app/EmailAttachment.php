<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailAttachment extends Model
{
    use Traits\SafeAttributesTrait;

    /*
     * mail_type : global | internal | external
     */
    protected $table = 'email_attachments';

    protected $fillable = [
        'id', 'mail_type','email_id','attachment'
    ];


    protected $safeAttributes = [
        'id', 'mail_type','email_id','attachment'
    ];
}
