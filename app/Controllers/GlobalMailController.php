<?php

namespace App\Http\Controllers;

use App\EmailAttachment;
use App\GlobalMail;
use App\Http\Requests\GlobalEmailForm;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class GlobalMailController extends Controller
{
    public function SendMailNet(GlobalEmailForm $request)
    {
        $request->request->add(['server'=> 'net']);
        $data = $this->SendMail($request);
        return $data;
    }

    public function SendMailPHP(GlobalEmailForm $request)
    {
        $request->request->add(['server'=> 'php']);
        $data = $this->SendMail($request);
        return $data;
    }

    /**
     * @param Request $request
     * @return array
     */
    public function SendMail(Request $request)
    {
        Log::info($request->all());
        $user_data = $request->all();
        $to = $request->to;
        $cc = $request->cc;
        $bcc = $request->bcc;
        $subject = $request->subject;
        $message = $request->message;
        $from = $request->from;
        $fileContents = $request->attachments;
        $mail_attachments = [];
        if(!$cc)
        {
            $cc = [];
        }

        if(!$bcc)
        {
            $bcc = [];
        }
        if(!$from)
        {
            $user_data['from'] = (($from) ? $from : env('ADMIN_EMAIL'));
        }
        try{

            $global_mail                = new GlobalMail();
            $global_mail->to_email      = (((is_array($to)) && (count($to) > 0)) ? implode(",",$to) : "");
            $global_mail->from_email    = (($from) ? $from : env('ADMIN_EMAIL'));
            $global_mail->cc_email      = ((count($cc) > 0) ? implode(",",$cc) : "");
            $global_mail->bcc_email     = ((count($bcc) > 0) ? implode(",",$bcc) : "");
            $global_mail->subject       = $subject;
            $global_mail->message       = $message;
            $global_mail->status        = 'draft';
            $global_mail->save();
            $file_path = '';
            if($fileContents)
            {

                foreach ( $fileContents as $item) {
                    if(is_string($item))
                    {
                        $file_path = $item;
                        $file_type= 'string';
                    } else {
                        $path = $item->getClientOriginalName();
                        $imagename = $item->getClientOriginalName();
                        if(Storage::disk('public_uploads')->exists('emails/'.$imagename))
                        {
                            $i=1;
                            $new_path=$path;
                            while (Storage::disk('public_uploads')->exists('emails/'.$new_path))
                            {
                                $extension = pathinfo($path, PATHINFO_EXTENSION);
                                $filename = pathinfo($path, PATHINFO_FILENAME);
                                $new_filename = $filename . '-' . $i . '.' . $extension;
                                $new_path = $new_filename;
                                $file_path = $new_path;
                                $i++;
                            }
                            $file_path = Storage::disk('public_uploads')->putFileAs('emails', new File($item),$file_path);
                        } else {
                            $file_path = Storage::disk('public_uploads')->putFileAs('emails', new File($item),$item->getClientOriginalName());
                        }
                        $file_type= 'file';
                    }
                    $filename                    = $file_path;
                    $mail_attachment             = new EmailAttachment();
                    $mail_attachment->mail_type  = 'global';
                    $mail_attachment->email_id   = $global_mail->id;
                    $mail_attachment->attachment = $filename;
                    $mail_attachment->type = $file_type;
                    $mail_attachment->save();
                    $mail_attachments[]        =  array('filename'=>$filename,'filetype'=>$file_type);
                }
            }
           $user_data['attachments'] = $mail_attachments;
//           $this->dispatch(new GlobalMail());
           Mail::to($to)
                ->cc($cc)
                ->bcc($bcc)
                ->queue(new \App\Mail\GlobalMail($user_data));

            GlobalMail::where('id',$global_mail->id)->update(['status'=>'success']);
            return ['status'=>'success','code'=>200,'data'=>''];

        }catch (Exception $exception)
        {
            GlobalMail::where('id',$global_mail->id)->update(['status'=>'failed']);
            return ['status'=>'failed','code'=>503,'data'=>''];
            Log::error($exception->getMessage());
        }
    }

    public function SendMailPHPInternal(Request $request)
    {
        $request->request->add(['server'=> 'php']);
        $data = $this->SendMail($request);
        return $data;
    }

    public function SendSMS(Request $request)
    {
        Log::info($request->to);
        $user_data = $request->all();
        $to = $request->to;
        $cc = $request->cc;
        $bcc = $request->bcc;
        $subject = $request->subject;
        $message = $request->message;
        $from = $request->from;
        $fileContents = $request->attachments;
        $mail_attachments = [];
        if(!$cc)
        {
            $cc = [];
        }

        if(!$bcc)
        {
            $bcc = [];
        }
        if(!$from)
        {
            $user_data['from'] = (($from) ? $from : env('ADMIN_EMAIL'));
        }
        try{

            $global_mail                = new GlobalMail();
            $global_mail->to_email      = (((is_array($to)) && (count($to) > 0)) ? implode(",",$to) : "");
            $global_mail->from_email    = (($from) ? $from : env('ADMIN_EMAIL'));
            $global_mail->cc_email      = ((count($cc) > 0) ? implode(",",$cc) : "");
            $global_mail->bcc_email     = ((count($bcc) > 0) ? implode(",",$bcc) : "");
            $global_mail->subject       = $subject;
            $global_mail->message       = $message;
            $global_mail->status        = 'draft';
            $global_mail->save();
            $file_path = '';
            if($fileContents)
            {

                foreach ( $fileContents as $item) {
                    if(is_string($item))
                    {
                        $file_path = $item;
                        $file_type= 'string';
                    } else {
                        $path = $item->getClientOriginalName();
                        $imagename = $item->getClientOriginalName();
                        if(Storage::disk('public_uploads')->exists('emails/'.$imagename))
                        {
                            $i=1;
                            $new_path=$path;
                            while (Storage::disk('public_uploads')->exists('emails/'.$new_path))
                            {
                                $extension = pathinfo($path, PATHINFO_EXTENSION);
                                $filename = pathinfo($path, PATHINFO_FILENAME);
                                $new_filename = $filename . '-' . $i . '.' . $extension;
                                $new_path = $new_filename;
                                $file_path = $new_path;
                                $i++;
                            }
                            $file_path = Storage::disk('public_uploads')->putFileAs('emails', new File($item),$file_path);
                        } else {
                            $file_path = Storage::disk('public_uploads')->putFileAs('emails', new File($item),$item->getClientOriginalName());
                        }
                        $file_type= 'file';
                    }
                    $filename                    = $file_path;
                    $mail_attachment             = new EmailAttachment();
                    $mail_attachment->mail_type  = 'global';
                    $mail_attachment->email_id   = $global_mail->id;
                    $mail_attachment->attachment = $filename;
                    $mail_attachment->type = $file_type;
                    $mail_attachment->save();
//                    $mail_attachments[]          =  $filename;
                    $mail_attachments[]        =  array('filename'=>$filename,'filetype'=>$file_type);
                }
            }
            $user_data['attachments'] = $mail_attachments;
            Mail::raw('Hi, welcome user!', function ($message) {
                $message->to(['ShuklaNisha@seasiainfotech.com'])
                ->subject('test sms message');
            });
//           $this->dispatch(new GlobalMail());
//            Mail::to($to)
//                ->cc($cc)
//                ->bcc($bcc)
//                ->send(new \App\Mail\GlobalMail($user_data));

            GlobalMail::where('id',$global_mail->id)->update(['status'=>'success']);
            return ['status'=>'success','code'=>200,'data'=>''];

        }catch (Exception $exception)
        {
            GlobalMail::where('id',$global_mail->id)->update(['status'=>'failed']);
            return ['status'=>'failed','code'=>503,'data'=>''];
            Log::error($exception->getMessage());
        }
    }

    public function SendMessage(Request $request)
    {
        try{
            $msg = 'avx';
            $to = 'ShuklaNisha@seasiainfotech.com';
            $mail = mail($to,"Test Mail",$msg);
            if($mail)
            {
                return ['status'=>'success','code'=>200,'data'=>'Mail Sent to '.$to];
            } else {
                return ['status'=>'error','code'=>500,'data'=> error_get_last()];
            }
        }catch (Exception $exception)
        {
            dd($exception->getMessage());
        }
    }
}
