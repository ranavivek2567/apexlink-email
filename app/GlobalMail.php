<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class GlobalMail extends Model
{
    use Notifiable;

    use Traits\SafeAttributesTrait;
//    protected $connection = 'mysql4';

    /**
     * server : php | net
     * status : success | draft | pending |failed
     */

    protected $fillable = [
        'id', 'to_email','from_email','cc_email','bcc_email','subject','message','server','status','updated_at','created_at'
    ];


    protected $safeAttributes = [
        'id', 'to_email','from_email','cc_email','bcc_email','subject','message','server','status','updated_at','created_at'
    ];

}
