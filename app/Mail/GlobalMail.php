<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Mockery\Exception;

class GlobalMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $mail_data;
    private $from_email;
    private $subject_email;
    private $message_email;
    private $attachments_email;
    public function __construct($mail_data)
    {
        $this->from_email = @$mail_data['from'];
        $this->subject_email = @$mail_data['subject'];
        $this->message_email = @$mail_data['message'];
        $this->attachments_email = @$mail_data['attachments'];

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        try{
            $from_email = (($this->from_email) ? $this->from_email : env('ADMIN_EMAIL'));
            $subject_email = $this->subject_email;
            $message_email = $this->message_email;
            $mail_send = $this->from($from_email)
                ->subject($subject_email)
                ->html($message_email);

            if($this->attachments_email)
            {

                foreach ( $this->attachments_email as $item)
                {
                    Log::info( $item);
                    if($item['filetype'] == 'file')
                    {
                        $file_path = public_path($item['filename']);
                    } else {
                        $file_path =$item['filename'];
                    }

                    $mail_send->attach($file_path);
                }
            }

        }catch (Exception $exception)
        {
            Log::error($exception->getMessage());
        }

    }


}
