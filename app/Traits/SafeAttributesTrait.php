<?php

namespace App\Traits;

use Illuminate\Support\Facades\Auth;

trait SafeAttributesTrait
{

    // the safe attributes list
    // it is also possible, to pass a callback function to check for user rights...
    // i.e. ['name','description', function($obj,$userObj){ return $userObj->allowedTo(Right::SAVE_DATA) }]
    #protected $safeAttributes = [];

    protected $safeAttributeNames = [];

    public function getSafeAttributeNames($userObj = null)
    {
        if (sizeof($this->safeAttributeNames) > 0 && $userObj == null) {
            return $this->safeAttributeNames;
        }

        $saveNames = [];

        if (is_null($this->safeAttributes)) {
            return $saveNames;
        }

        // fix for single-setted save attributes
        if (is_array($this->safeAttributes) && (!is_array(current($this->safeAttributes)))) {
            $this->safeAttributes = [$this->safeAttributes];
        }

        foreach ($this->safeAttributes as $attrList) {
            $attrFuncCheck = null;
            // check, if last attr is a function
            if (is_callable(end($attrList))) {
                // remove last array entry
                $attrFuncCheck = array_pop($attrList);
            }

            // save name values if no func is given or func return is valid (true)
            if ($attrFuncCheck == null || call_user_func($attrFuncCheck, $this, $userObj)) {
                $saveNames = array_merge($saveNames, $attrList);
            }
        }

        // uniq the array
        $saveNames = array_unique($saveNames);

        // save the defaultAttributeNames, if no user is given
        if ($userObj == null) {
            $this->safeAttributeNames = $saveNames;
        }

        // return the save names
        return $saveNames;
    }

    public function isSafeAttribute($inputAttrName, $userObj = null)
    {
        return in_array($inputAttrName, $this->getSafeAttributeNames($userObj));
    }

    public function deleteUnsafeAttributes($inputAttrData, $userObj = null)
    {
        return array_intersect_key(
            $inputAttrData,
            array_flip($this->getSafeAttributeNames($userObj))
        );
    }

    public function setSafeAttributes($inputAttrData, $userObj = null)
    {
        if ($userObj == null) {
            //$userObj = \App\User::current();
            $userObj = Auth::user();
        }
        // fill real save attributes
        return $this->forceFill(
            $this->deleteUnsafeAttributes($inputAttrData, $userObj)
        );
    }

}


?>