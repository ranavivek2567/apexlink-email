<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class GlobalEmailForm extends FormRequest
{

    public function __construct() {
        \Illuminate\Support\Facades\Validator::extend("emails", function($attribute, $value, $parameters) {
            $rules = [
                'email' => 'required|email',
            ];


            if(is_array($value))
            {
                foreach ($value as $email) {
                    $data = [
                        'email' => $email
                    ];
                    $validator = \Illuminate\Support\Facades\Validator::make($data, $rules);
                    if ($validator->fails()) {
                        return false;
                    }
                }
            } else {
                //$rules['to'] = 'required|array';
                $data = [
                    'email' => 'array'
                ];
                $validator = \Illuminate\Support\Facades\Validator::make($data, $rules);
                if ($validator->fails()) {
                    return false;
                }
            }

            return true;
        });
    }



    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {

        $rules = [
            'to' => 'required|emails|array',
            'from' => 'email',
            'cc' => 'array|emails',
            'bcc' => 'array|emails',
            'attachments' => 'array',
            'subject' => 'required|max:100',
            'message' => 'required'
        ];

        return $rules;
    }

    public function messages()
    {
        $messages = [
            'to.emails' => 'Please Enter Valid Email',
            'to.array' => 'Format must be an array',
            'from.emails' => 'Please Enter Valid Email',
            'cc.emails' => 'Please Enter Valid Email',
            'cc.array' => 'Format must be an array',
            'bcc.emails' => 'Please Enter Valid Email',
            'bcc.array' => 'Format must be an array',
        ];

        return $messages;

    }

    protected function failedValidation(Validator $validator) {

        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }


}
