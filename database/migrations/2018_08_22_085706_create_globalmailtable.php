<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGlobalmailtable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('global_mails', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('to_email');
            $table->text('from_email');
            $table->text('cc_email')->nullable();
            $table->text('bcc_email')->nullable();
            $table->string('subject')->nullable();
            $table->string('attachment')->nullable();
            $table->text('message')->nullable();
            $table->enum('server', ['php','net']);
            $table->enum('status', ['success','draft','pending','failed']);
            $table->timestamps();
        });

        Schema::create('internal_mails', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('to_email');
            $table->text('from_email');
            $table->text('cc_email')->nullable();
            $table->text('bcc_email')->nullable();
            $table->string('subject')->nullable();
            $table->string('attachment')->nullable();
            $table->text('message')->nullable();
            $table->text('function_name')->nullable();
            $table->enum('status', ['success','draft','pending','failed']);
            $table->timestamps();
        });

        Schema::create('email_attachments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('mail_type', ['global','internal','external']);
            $table->integer('email_id');
            $table->string('attachment')->nullable();
            $table->enum('type', ['string','file']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('global_mails');
        Schema::dropIfExists('internal_mails');
        Schema::dropIfExists('email_attachments');

    }
}
